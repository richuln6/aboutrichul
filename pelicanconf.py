#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Richul'
SITENAME = "Richul's Blog"
SITEURL = 'https://richuln6.gitlab.io/aboutrichul'

PATH = 'content'

TIMEZONE = 'UTC'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         )

# Social widget
SOCIAL = (('Github', 'https://github.com/richuln6'),
          ('Instagram', 'https://instagram.com/richulll'),
('Linked In','https://www.linkedin.com/in/richul398/'))

DEFAULT_PAGINATION = False

OUTPUT_PATH= 'public/'
PLUGIN_PATHS=['plugins/i18n_subsites/',]
PLUGINS=['i18n_subsites',]
JINJA_ENVIRONMENT={'extensions':['jinja2.ext.i18n']}
BOOTSTRAP_THEME='flatly'
PYGMENTS_STYLE='monokai'
PATH='content'
THEME='theme/pelican-bootstrap3'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
